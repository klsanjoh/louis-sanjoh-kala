# Description

Bash script to automate tasks

## Requirement

Freshly installed Ubuntu server 18.04

Run script as root

## The script will run the following
install the latest updates on the machine.

Install Nginx and configure for php.

install php7.4 with fpm (latest compatible version with WP 5.4).

install php7.4 extensions (php7.4-cli php7.4-json php7.4-common php7.4-mysql php7.4-zip php7.4-gd php7.4-mbstring php7.4-curl php7.4-xml php7.4-bcmath php7.4-imagick php7.4-exif)

install mariadb

Firewall default rule (block all incoming traffic and allow outgoing traffic)

Firewall open port 22 for internal ssh access

Firewall open port 80 for incoming traffic

Firewall port 8088 only accessible from the internal network

Enable firewall

Download and extract WordPress

Install WordPress Cli 

Create wp-config file with user, database and password

Create database with credentials of wp-config file

Create user with sudo nopasswd rights

Generate keys for the newly created user grant owner and permission

Disallow root access through ssh

## Notes
I could not achieve limiting port 80 only to WordPress App.

The script can be improved for security and efficiency.


